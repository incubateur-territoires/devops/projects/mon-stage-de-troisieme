resource "random_password" "redis_password" {
  length  = 32
  special = false
}

module "redis" {
  source         = "gitlab.com/vigigloo/tools-k8s/redis"
  version        = "0.1.1"
  chart_name     = "redis"
  chart_version  = "16.13.2"
  namespace      = module.namespace.namespace
  redis_password = resource.random_password.redis_password.result
  redis_replicas = 0

  requests_cpu    = "50m"
  requests_memory = "512Mi"
  limits_cpu      = null
  limits_memory   = "512Mi"
}

module "memcached" {
  source        = "gitlab.com/vigigloo/tools-k8s/memcached"
  version       = "0.1.1"
  chart_name    = "memcached"
  chart_version = "6.6.2"
  namespace     = module.namespace.namespace

  requests_cpu    = "20m"
  requests_memory = "256Mi"
  limits_cpu      = null
  limits_memory   = "256Mi"
}
