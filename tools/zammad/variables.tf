variable "hostname" {
  type = string
}

variable "project_slug" {
  type = string
}

variable "scaleway_organization_id" {
  type = string
}
variable "scaleway_project_id" {
  type = string
}
variable "scaleway_access_key" {
  type = string
}
variable "scaleway_secret_key" {
  type      = string
  sensitive = true
}
variable "scaleway_project_config" {
  type = object({
    project_id = string
    access_key = string
    secret_key = string
  })
  sensitive = true
}
variable "snapshot_volume_handle" {
  type        = string
  description = "For snapshoting the volume that stores Zammad's attachments. Typically starts with fr-par-1/..."
}
