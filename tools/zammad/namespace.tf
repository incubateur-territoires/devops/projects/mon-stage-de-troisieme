module "namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "2.0.2"
  max_cpu_requests  = 14
  max_memory_limits = "21Gi"
  namespace         = var.project_slug
  project_name      = "Zammad"
  project_slug      = "zammad"

  default_container_cpu_requests  = "25m"
  default_container_memory_limits = "128Mi"
}
