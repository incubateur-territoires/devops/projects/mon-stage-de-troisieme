# tflint-ignore: terraform_unused_declarations
data "scaleway_k8s_cluster" "prod" {
  provider   = scaleway.scaleway_production
  cluster_id = var.scaleway_cluster_production_cluster_id
}

# tflint-ignore: terraform_unused_declarations
data "scaleway_k8s_cluster" "dev" {
  provider   = scaleway.scaleway_development
  cluster_id = var.scaleway_cluster_development_cluster_id
}

#data "gitlab_project" "<name>" {
#  id = <id>
#}
#
#
#data "gitlab_group" "<name>" {
#  group_id = <id>
#}
