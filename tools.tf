module "zammad" {
  source     = "./tools/zammad"
  kubeconfig = data.scaleway_k8s_cluster.prod.kubeconfig[0]
  hostname   = "zammad.${var.prod_base_domain}"

  project_slug = "${var.project_slug}-zammad"

  scaleway_project_id      = var.scaleway_cluster_production_project_id
  scaleway_organization_id = var.scaleway_organization_id
  scaleway_access_key      = var.scaleway_cluster_production_access_key
  scaleway_secret_key      = var.scaleway_cluster_production_secret_key
  scaleway_project_config  = var.scaleway_project_config
  snapshot_volume_handle   = var.zammad_snapshot_volume_handle

  providers = {
    scaleway = scaleway.scaleway_project
  }
}
